import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule, registerLocaleData } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// NG_ZORRO
import en from '@angular/common/locales/en';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_US } from 'ng-zorro-antd/i18n';

// PAGES
import { LayoutComponent } from './pages/layout/layout.component';
import { ProductDetailComponent } from './pages/product-detail/product-detail.component';
import { ProductListComponent } from './pages/product-list/product-list.component';
import { DeletedProductListComponent } from './pages/deleted-product-list/deleted-product-list.component';
import { SharedModule } from './components/shated.module';
import { AddEditProductComponent } from './pages/add-edit-product/add-edit-product.component';

registerLocaleData(en);

const PAGE_COMPONENTS = [
  LayoutComponent,
    ProductDetailComponent,
    ProductListComponent,
    DeletedProductListComponent,
    AddEditProductComponent
]

@NgModule({
  declarations: [
    AppComponent,
    ...PAGE_COMPONENTS
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    SharedModule,
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
