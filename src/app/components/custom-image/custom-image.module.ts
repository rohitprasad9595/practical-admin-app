import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomImageComponent } from './custom-image.component';



@NgModule({
  declarations: [
    CustomImageComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CustomImageModule { }
