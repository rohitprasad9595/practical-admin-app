import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomBreadcrumbComponent } from './custom-breadcrumb.component';



@NgModule({
  declarations: [
    CustomBreadcrumbComponent
  ],
  imports: [
    CommonModule
  ]
})
export class CustomBreadcrumbModule { }
