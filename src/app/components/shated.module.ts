import { NgModule } from '@angular/core';

import { CustomBreadcrumbComponent } from './custom-breadcrumb/custom-breadcrumb.component';
import { CustomImageComponent } from './custom-image/custom-image.component';

import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzBreadCrumbModule } from 'ng-zorro-antd/breadcrumb';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzAvatarModule } from 'ng-zorro-antd/avatar';
import { NzRateModule } from 'ng-zorro-antd/rate';
import { NzImageModule } from 'ng-zorro-antd/image';
import { NzDescriptionsModule } from 'ng-zorro-antd/descriptions';
import { NzSliderModule } from 'ng-zorro-antd/slider';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzRadioModule } from 'ng-zorro-antd/radio';
import { NzSelectModule } from 'ng-zorro-antd/select';
import { NzSpinModule } from 'ng-zorro-antd/spin';
import { NzPageHeaderModule } from 'ng-zorro-antd/page-header';
import { IconsProviderModule } from './icons-provider.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

const BASE_MODULES = [CommonModule, FormsModule, ReactiveFormsModule];

const NZ_MODULES = [
    NzLayoutModule,
    NzFormModule,
    NzMenuModule,
    NzButtonModule,
    NzInputModule,
    NzSliderModule,
    NzGridModule,
    NzBreadCrumbModule,
    NzSpinModule,
    NzCardModule,
    NzPageHeaderModule,
    NzAvatarModule,
    NzRateModule,
    NzImageModule,
    NzDescriptionsModule,
    NzRadioModule,
    NzSelectModule,
    IconsProviderModule
];

  const SHARED_COMPONENTS = [CustomBreadcrumbComponent,  CustomImageComponent]

  @NgModule({
    declarations: [...SHARED_COMPONENTS],
    imports: [ ...BASE_MODULES,...NZ_MODULES],
    exports: [ ...BASE_MODULES,...NZ_MODULES,  ...SHARED_COMPONENTS],
    // entryComponents: [...ENTRY_COMPONENTS],
  })

  export class SharedModule {}
