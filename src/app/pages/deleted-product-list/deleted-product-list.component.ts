import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {  Router } from '@angular/router';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Subscription } from 'rxjs';
@Component({
  selector: 'app-deleted-product-list',
  providers: [NzModalService, NzNotificationService],
  templateUrl: './deleted-product-list.component.html',
  styleUrls: ['./deleted-product-list.component.scss']
})
export class DeletedProductListComponent implements OnInit, OnDestroy {
  public products: any;
  public isLoading: boolean = false;
  private subscription: Subscription = new Subscription();
  confirmModal?: NzModalRef; 
  constructor(private http: HttpClient, private route: Router, private fb: FormBuilder,
    private modal: NzModalService, private notification: NzNotificationService) {
  }

  ngOnInit(): void {
    this.getAllProducts()
  }

public showConfirm(id: number): void {
  this.confirmModal = this.modal.confirm({
    nzTitle: 'Do you Want to delete these product?',
    nzContent: 'After deleting you wll not be able to restore the product.',
    nzOnOk: (() => this.deleteProduct(id))
  });
}

public showRestoreConfirm(id: number): void {
  this.confirmModal = this.modal.confirm({
    nzTitle: 'Do you Want to restore these product?',
    nzContent: 'After restoring the product the product will be visible in products list.',
    nzOnOk: (() => this.restoreProduct(id))
  });
}


private deleteProduct(id: number): void {
  this.isLoading = true;  
  this.subscription.add(this.http.delete(`/api/products/${id}`).subscribe(res => {
      this.products = res;
      this.isLoading = false;
      this.notification.create(
        'success',
        'Product deleted',
        'Product has been deleted successfully.'
      );
      this.getAllProducts();
    }))
      
}

private restoreProduct(id: number): void {
  this.isLoading = true;
  this.subscription.add(this.http.patch(`/api/products/${id}`, {remove: false}).subscribe(res => {
    this.products = res;
    this.isLoading = false;
    this.notification.create(
      'success',
      'Product restored',
      'Product has been restored successfully.'
    );
    this.getAllProducts();
  }))

}

  public navigate(id: string): void {
    this.route.navigate([`/detail/${id}`])
  }
  public getAllProducts(): void {
    this.isLoading = true;
    this.subscription.add(this.http.get(`/api/products`).subscribe(res => {
      this.products = res;
      this.isLoading = false;
    }));
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

}
