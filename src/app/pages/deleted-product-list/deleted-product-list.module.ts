import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DeletedProductListComponent } from './deleted-product-list.component';



@NgModule({
  declarations: [
    DeletedProductListComponent
  ],
  imports: [
    CommonModule
  ]
})
export class DeletedProductListModule { }
