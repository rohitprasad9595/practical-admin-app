import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeletedProductListComponent } from './deleted-product-list.component';

describe('DeletedProductListComponent', () => {
  let component: DeletedProductListComponent;
  let fixture: ComponentFixture<DeletedProductListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeletedProductListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DeletedProductListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
