import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.component.html',
  styleUrls: ['./product-detail.component.scss']
})
export class ProductDetailComponent implements OnInit, OnDestroy {
  private productId: any = '';
  public localpath = '/assets/img/'
  private subscription: Subscription = new Subscription() ;
  public productDetail: any = {}
  constructor(private http: HttpClient, private router: ActivatedRoute) { }

  ngOnInit(): void {
    this.productId = this.router.snapshot.paramMap.get('id')
    this.subscription.add(this.http.get(`/api/products/${this.productId}`).subscribe(res => {
      this.productDetail = res
    }))
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }


}
