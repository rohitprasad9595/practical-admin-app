import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import {  Router } from '@angular/router';
import {
  debounceTime,
  map,
  distinctUntilChanged,
  filter
} from "rxjs/operators";
import { fromEvent, Subscription } from 'rxjs';
import { NzModalRef, NzModalService } from 'ng-zorro-antd/modal';
import { NzNotificationService } from 'ng-zorro-antd/notification';
@Component({
  selector: 'app-product-list',
  providers:[NzModalService, NzNotificationService],
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {
  public products: any;
  public isLoading: boolean = false;
  private subscription: Subscription = new Subscription();
  @ViewChild('searchInput', { static: true }) searchInput: any;
  
  public confirmModal?: NzModalRef; 
  public filterForm: FormGroup;
  checkOptionsOne = [
    { label: 'Apple', value: 'Apple', checked: true },
    { label: 'Pear', value: 'Pear' },
    { label: 'Orange', value: 'Orange' }
  ];
  constructor(private http: HttpClient, private route: Router, private fb: FormBuilder,
    private modal: NzModalService, private notification: NzNotificationService) { 
    this.filterForm = this.fb.group({
      in_stock: new FormControl(null),
      rating: new FormControl(null),
      price: new FormControl(null),
      category: new FormControl(null)
    })
  }

  ngOnInit(): void {
    this.getAllProducts()
    
  fromEvent(this.searchInput.nativeElement, 'keyup').pipe(
    // get value
    map((event: any) => {
      return event.target.value;
    })
    
    // Time in milliseconds between key events
    , debounceTime(1000)

    // If previous query is diffent from current   
    , distinctUntilChanged()

    // subscription for response
  ).subscribe((text: string) => {
    this.isLoading = true;
    // this.isSearching = true;
    const searchTerm = typeof text ===
     'string' ? text.trim()  : text
      this.subscription.add(this.http.get(`/api/products?q=${searchTerm}`).subscribe(res => {
        this.products = res;
        this.isLoading = false;
      }));
    
  });

  }

public showConfirm(id: number): void {
  this.confirmModal = this.modal.confirm({
    nzTitle: 'Do you Want to delete these product?',
    nzContent: 'After deleting the product you can restore product from trash menu.',
    nzOnOk: (() => this.deleteProduct(id))
  });
}

private deleteProduct(id: number): void {
  this.isLoading = true;
  this.subscription.add(this.http.patch(`/api/products/${id}`,{"remove": true}).subscribe(res => {
      this.products = res;
      this.notification.create(
        'success',
        'Product deleted',
        'Product has been removed successfully and move to trash menu successfully.'
      );
      this.getAllProducts();
      this.isLoading = false;
    }))
    
}

  // // convenience getter for easy access to form fields
  get f() { return this.filterForm.controls; }

  public navigate(id: string): void {
    this.route.navigate([`/detail/${id}`])
  }
  public getAllProducts(): void {
    this.isLoading = true;
    this.subscription.add(this.http.get(`/api/products`).subscribe(res => {
      this.isLoading = false;
      this.products = res;
    }));
  }

  public getFilteredData(): void {
    this.isLoading = true;
    let params = {
      in_stock : this.f.in_stock.value,
      price : this.f.price.value,
      rating : this.f.rating.value,
      category : this.f.category.value,
    }
    let filteredQuery = ''
    if(params.in_stock) filteredQuery +=`in_stock=${params.in_stock}&` 
    if(params.price) filteredQuery+= `price_gte=${params.price[0]}&price_lte=${params.price[0]}&` 
    if(params.rating) filteredQuery+= `rating=${params.rating}&` 
    if(params.category) filteredQuery+= `category=${params.category}&`
     
    this.subscription.add(this.http.get(`/api/products?${filteredQuery}`).subscribe(res => {
      this.products = res
      this.isLoading = false;
    }))
  }

  public resetForm(): void {
    this.filterForm.reset();
   this.getAllProducts();
  }

  public ngOnDestroy(): void {
    this.subscription.unsubscribe()
  }

}
