import { HttpClient } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import {  NzNotificationService } from 'ng-zorro-antd/notification';
import {NzModalRef, NzModalService} from 'ng-zorro-antd/modal';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-add-edit-product',
  providers:[NzNotificationService, NzModalService],
  templateUrl: './add-edit-product.component.html',
  styleUrls: ['./add-edit-product.component.scss']
})
export class AddEditProductComponent implements OnInit, OnDestroy {
  private subscription: Subscription = new Subscription();
  private productId: any = '';
  public productDetail: any;
  // form instance
  public validateForm: FormGroup;

    // confirmation modal instance
    // private confirmModal: NzModalRef;

  // form submit stauts
  public submitted: boolean = false;

  // submit loading
  public isLoadingOne = false;

  // error messages
  public validation_messages = {
    'title': [
      { type: 'required', message: 'Please input product title!' },
      { type: 'maxlength', message: 'Product title should not exceed more than 50 characters long!' }
    ],
    'description': [
      { type: 'required', message: 'Please input product description!' },
      { type: 'minlength', message: 'Product description should not exceed more than 150 characters long!' }
    ],
    'category': [
      { type: 'required', message: 'Please select product category!' },
      { type: 'minlength', message: 'Product description should not exceed more than 150 characters long!' }
    ],
    'price': [
      { type: 'required', message: 'Please input product price!' },
      { type: 'pattern', message: 'Product price should be in proper format!' }
    ],
    'image': [
      { type: 'required', message: 'Please upload product image!' }
    ],
  }

  constructor(private http: HttpClient, private router:ActivatedRoute, private route: Router,
    private fb: FormBuilder, private notification: NzNotificationService,
    private modal: NzModalService) {
      this.validateForm = this.fb.group({
        title: new FormControl(null, {
          validators: Validators.compose([Validators.maxLength(50), Validators.required]),
          updateOn:'blur'
        }),
        description: new FormControl(null, {
          validators: Validators.compose([Validators.maxLength(150), Validators.required]),
          updateOn:'blur'
        }),
        in_stock: new FormControl(true, {validators: Validators.compose([Validators.required])}),
        rating: new FormControl(null),
        image: new FormControl(null, {validators: Validators.compose([Validators.required])}),
        price: new FormControl(null, {validators:Validators.compose([Validators.required, Validators.pattern(/^\d{0,8}(\.\d{1,4})?$/)])}),
        category: new FormControl(null, {validators: Validators.compose([Validators.required])})
      })
   }

  ngOnInit(): void {
    this.productId = this.router.snapshot.paramMap.get('id')

    if(this.productId) {
      this.getProductData()
    }
  }

  onFileChange(event: any) {
    console.log(event.target.files, '....files')
    if (event.target.files.length > 0) {
      const file = event.target.files[0].name;
      this.validateForm.patchValue({
        image: file
      });
    }
  }

  public getProductData(): void {
    this.subscription.add(this.http.get(`/api/products/${this.productId}`).subscribe(res => {
      this.productDetail = res;
      this.validateForm.setValue({
          title: this.productDetail.name,
          description: this.productDetail.description,
          rating:this.productDetail.rating,
          in_stock:this.productDetail.in_stock,
          price:this.productDetail.price,
          category: this.productDetail.category,
          image: this.productDetail.image,
      })
    }))
  }

    // convenience getter for easy access to form fields
    get f() { return this.validateForm.controls; }

    // routing to listing page
  public goBack(): void {

    // form submission ststus
    this.submitted = false;

    // reset the form
    this.validateForm.reset();

    // mark the fields pristine
    for (const key in this.f) {
      this.f[key].markAsPristine();
      this.f[key].updateValueAndValidity();
    }

    this.route.navigate(['/list']);
  }

  public resetForm(): void{
      this.validateForm.reset()
      for(const key in this.f) {
        this.f[key].markAsPristine();
        this.f[key].updateValueAndValidity();
      }
  }

  private addProduct(): void {
    const params = {
      name: this.f.title.value,
        description: this.f.description.value,
        rating: this.f.rating.value,
        price: this.f.price.value,
        in_stock: Boolean(this.f.in_stock.value),
      category: this.f.category.value,
      image: this.f.image.value,
      remove: false
    }
    

    this.subscription.add(this.http.post(`/api/products`, params).subscribe(res => {
        
          // success modal when status updated
          this.notification.create(
            'success',
            'Product added',
            'Product has been updated successfully.'
          );
          // if succesfully updated then go back to listing page
          this.goBack();
    }, (error) => {

      // print the error on console
      console.error(error);
    }));
  }

  private updateProduct(): void {
    const params = {
      id:this.productId,
      name: this.f.title.value,
        description: this.f.description.value,
        rating: this.f.rating.value,
        price: this.f.price.value,
        in_stock: Boolean(this.f.in_stock.value),
      category: this.f.category.value,
      image: this.f.image.value,
      remove: false
    }

    this.subscription.add(this.http.put(`/api/products/${this.productId}`, params).subscribe(res => {
          // success modal when status updated
          this.notification.create(
            'success',
            'Product Updated',
            'Product has been updated successfully.'
          );
          // if succesfully updated then go back to listing page
          this.goBack();
    }, (error) => {

      // print the error on console
      console.error(error);
    }));  
  }

  public submitForm(): void{
    for(const key in this.f) {
      this.f[key].markAsDirty();
      this.f[key].updateValueAndValidity();
    }
    this.submitted = true;
     // stop here if form is invalid
     if (this.validateForm.invalid) {
      return;
    }

    if(this.productId) {
      this.updateProduct();
    } else {
      this.addProduct();
    }


}

// unsubscriobe all the apis
ngOnDestroy() {
  this.subscription.unsubscribe();
}

}
